Play
====

A simple program to run steam games from the command line. Meant for use with dmenu, etc.

Features
--------
- Launches steam minimized to tray if it's not already running, then runs your game.
- Does fuzzy matching on partial input, so `play bor 2` -> runs Borderlands 2
  - words can be partial but have to be in order (so `play 2 bor` ain't gonna work)
- Reads from steam's appmanifest files, not from `steam_appid.txt`, so it works with games that don't have one of those.
- Pretty fast (but steam is still slow, so...)
- Talks to you in a depressed robot voice

Requirements
------------
Steam:
- Steam should be found at `/usr/bin/steam-runtime`. If it's not there, change the path in `src/main.rs` to match your steam path and recompile.
- Looks for steam games in `$HOME/.local/share/Steam`. If that's not where you put them change the path in `src/main.rs` and recompile.

Espeak:
- if you want it to talk to you. Otherwise it will probably be quiet or crap out or something.

Rust:
- `rust-src` - for minimal binaries
- nightly toolchain

Configuration
-------------
Edit code to suit your tastes, recompile.

Install
-------
To make a release build and install to `/usr/local/bin/play`:
```sh
make && sudo make install
```

License
-------
GPL v3.0 - see [LICENSE](LICENSE)
