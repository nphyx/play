use logos::Logos;

#[derive(Logos, Debug, PartialEq)]
enum Token {
    #[regex(r#""[0-9]+""#, priority = 2)]
    Number,
    #[regex(r#""[^"]+""#, priority = 1)]
    Text,
    #[token(r#""AppState""#)]
    Preamble,
    #[token("{")]
    OpenDelim,
    #[token("}")]
    CloseDelim,
    #[token(r#""appid""#)]
    AppId,
    #[token(r#""Universe""#)]
    Universe,
    #[token(r#""name""#)]
    Name,
    #[token(r#""StateFlags""#)]
    StateFlags,
    #[token(r#""installdir""#)]
    InstallDir,
    #[token(r#""LastUpdated""#)]
    LastUpdated,
    #[token(r#""UpdateResult""#)]
    UpdateResult,
    #[token(r#""SizeOnDisk""#)]
    SizeOnDisk,
    #[token(r#""buildid""#)]
    BuildId,
    #[token(r#""LastOwner""#)]
    LastOwner,
    #[token(r#""BytesToDownload""#)]
    BytesToDownload,
    #[token(r#""BytesDownloaded""#)]
    BytesDownloaded,
    #[token(r#""BytesToStage""#)]
    BytesToStage,
    #[token(r#""BytesStaged""#)]
    BytesStaged,
    #[token(r#""AutoUpdateBehavior""#)]
    AutoUpdateBehavior,
    #[token(r#""AllowOtherDownloadsWhileRunning""#)]
    AllowOtherDownloadsWhileRunning,
    #[token(r#""ScheduledAutoUpdate""#)]
    ScheduledAutoUpdate,
    #[token(r#""InstalledDepots""#)]
    InstalledDepots,
    #[token(r#""UserConfig""#)]
    UserConfig,
    #[token(r#""manifest""#)]
    Manifest,
    #[token(r#""language""#)]
    Language,
    #[regex("[ \t]+", logos::skip)]
    Whitespace,
    #[error]
    Error,
}

fn clean(string: String) -> String {
    string.replace('"', "")
}

#[test]
fn test_token() {
    let mut lex = Token::lexer(r#"     "appid""#);
    assert_eq!(lex.next(), Some(Token::AppId));
    assert_eq!(lex.slice(), r#""appid""#);
}

#[test]
fn test_number_value() {
    let mut lex = Token::lexer(r#"     "appid"     "10000000""#);
    assert_eq!(lex.next(), Some(Token::AppId));
    assert_eq!(lex.slice(), r#""appid""#);
    assert_eq!(lex.next(), Some(Token::Number));
    assert_eq!(clean(lex.slice().into()).parse::<i64>().unwrap(), 10000000);
}

#[test]
fn test_string_value() {
    let mut lex = Token::lexer(r#"     "name"     "My Cool Game""#);
    assert_eq!(lex.next(), Some(Token::Name));
    assert_eq!(lex.slice(), r#""name""#);
    assert_eq!(lex.next(), Some(Token::Text));
    assert_eq!(clean(lex.slice().into()), "My Cool Game");
}

fn parse_line(line: &str) -> Vec<Item> {
    let mut lex = Token::lexer(line);
    let mut items: Vec<Item> = Vec::new();
    while let Some(item) = lex.next() {
        match item {
            Token::Text => items.push(Item::Text(clean(lex.slice().to_string()))),
            Token::Number => {
                let strnum = clean(lex.slice().to_string());
                let num = strnum.parse::<i64>().unwrap();
                items.push(Item::Number(num));
            }
            Token::Error => {}
            token => items.push(Item::Token(token)),
        }
    }
    items
}

#[test]
fn test_parse_line() {
    let items = parse_line(r#"  "appid"     "1234""#);
    assert_eq!(items, vec![Item::Token(Token::AppId), Item::Number(1234)]);
}

#[derive(Debug, PartialEq)]
enum Item {
    Token(Token),
    Text(String),
    Number(i64),
}

/*
 * For if I ever want to parse a whole app manifest ??
#[derive(Default, Debug)]
struct UserConfig {
    language: String,
}

#[derive(Default, Debug)]
struct InstalledDepots {
    manifest: String,
    size: i64,
}

#[derive(Default, Debug)]
pub struct AppState {
    appid: i64,
    universe: i64,
    name: String,
    state_flags: i64,
    installdir: String,
    last_updated: i64,
    update_result: i64,
    size_on_disk: i64,
    buildid: i64,
    last_owner: String,
    bytes_to_download: i64,
    bytes_to_stage: i64,
    auto_update_behavior: i64,
    allow_other_downloads_while_running: i64,
    scheduled_auto_update: i64,
    install_depots: std::collections::HashMap<i64, Vec<InstalledDepots>>,
    user_config: UserConfig,
}

#[allow(dead_code)]
impl AppState {
    fn from_file(path: &std::path::Path) -> Result<AppState, std::io::Error> {
        use std::fs::File;
        use std::io::{BufRead, BufReader};
        assert!(path.is_file());
        let mut reader = BufReader::new(File::open(path)?);
        let mut line: String = String::new();
        let mut app: AppState = Default::default();
        while reader.read_line(&mut line).is_ok() {
            match &parse_line(&line)[..] {
                [Item::Token(Token::AppId), Item::Number(n)] => app.appid = *n,
                [Item::Token(Token::Universe), Item::Number(n)] => app.universe = *n,
                [Item::Token(Token::Name), Item::Text(s)] => app.name = s.clone(),
                [Item::Token(Token::StateFlags), Item::Number(n)] => app.state_flags = *n,
                [Item::Token(Token::InstallDir), Item::Text(s)] => app.name = s.clone(),
                // TODO rest of those ... jeez
                _ => {}
            }
        }
        Ok(app)
    }
}
*/

#[derive(Default)]
pub struct GameAppId {
    pub app_id: i64,
    pub name: String,
    pub tokens: Vec<String>,
}

impl GameAppId {
    fn is_complete(&self) -> bool {
        self.app_id > 0 && !self.name.is_empty() && !self.tokens.is_empty()
    }
    pub fn from_file(path: &std::path::Path) -> Result<GameAppId, std::io::Error> {
        use std::fs::File;
        use std::io::{BufRead, BufReader};
        assert!(path.is_file());
        let reader = BufReader::new(File::open(path)?);
        let mut game: GameAppId = Default::default();
        'reading: for line in reader.lines() {
            match &parse_line(&line.unwrap_or_else(|_| "".to_string()))[..] {
                [Item::Token(Token::AppId), Item::Number(n)] => game.app_id = *n,
                [Item::Token(Token::Name), Item::Text(s)] => {
                    game.name = s.clone();
                    game.tokens = s.to_lowercase().split(' ').map(|s| s.to_string()).collect();
                }
                _ => {
                    if game.is_complete() {
                        break 'reading;
                    }
                }
            }
        }
        Ok(game)
    }
}
