use std::collections::HashMap;
use std::io::{self, Write};
use std::process::Command;

mod app_manifest;
use app_manifest::GameAppId;

type GameList = HashMap<i64, GameAppId>;

fn get_games() -> Result<GameList, std::io::Error> {
    let mut games: GameList = HashMap::new();
    let home_dir = dirs::home_dir().expect("No home directory found :(");
    let steam_apps = home_dir.join(".local/share/Steam/steamapps");
    if steam_apps.is_dir() {
        for entry in std::fs::read_dir(steam_apps)? {
            let file = entry?;
            let maybe_manifest = file.path();
            if maybe_manifest.is_file() {
                let fname: String = maybe_manifest.file_name().unwrap().to_str().unwrap().into();
                if maybe_manifest.is_file() && fname.contains("appmanifest") {
                    let game = GameAppId::from_file(&maybe_manifest)?;
                    games.insert(game.app_id, game);
                }
            }
        }
    }
    Ok(games)
}

fn find_game(games: &GameList, search_tokens: Vec<String>) -> Option<i64> {
    'search: for game in games.values() {
        if game.tokens.len() < search_tokens.len() {
            continue;
        }
        for (i, token) in search_tokens.iter().enumerate() {
            let paired = game.tokens.get(i).expect("token length mismatch");
            if !paired.contains(token) {
                continue 'search;
            }
        }
        return Some(game.app_id);
    }
    None
}

fn speak(string: String) {
    let output = Command::new("/usr/bin/espeak")
        .arg(string)
        .output()
        .expect("i'm mute");
    io::stdout().write_all(&output.stdout).unwrap();
}

fn run_game(appid: i64) {
    let output = Command::new("/usr/bin/steam-runtime")
        .arg("-silent")
        .arg("-nofriendsui")
        .arg(format!("steam://run/{}", appid))
        .output()
        .expect("steam failed to run :(");
    io::stdout().write_all(&output.stdout).unwrap();
}

fn print_games(games: &GameList) {
    // titles which will be excluded from the list
    let excluded = vec!["Proton", "Steam Linux", "Steamworks"];
    'list: for (_id, game) in games.iter() {
        for partial in &excluded {
            if game.name.starts_with(partial) {
                continue 'list;
            }
        }
        println!("{}", game.name);
    }
}

fn main() -> Result<(), std::io::Error> {
    let games = get_games()?;
    let mut args = pico_args::Arguments::from_env();
    if args.contains("--list") {
        print_games(&games);
        return Ok(());
    }
    let search_tokens: Vec<String> = args
        .free()
        .expect("please enter a game name")
        .iter()
        .map(|s| s.to_lowercase())
        .collect();
    if search_tokens.is_empty() {
        speak("i don't want to".into());
        return Ok(());
    }
    match find_game(&games, search_tokens) {
        Some(id) => {
            let game = games.get(&id).unwrap();
            speak(format!("playing {}", game.name));
            run_game(id);
        }
        None => speak("why don't you go play with yourself".into()),
    }
    Ok(())
}
